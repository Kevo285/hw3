﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    private int count;
    public Text CountText;
    public Text Wintext;

    // Update is called once per frame
    void Start()
    {
        count = 0;
        rb = GetComponent<Rigidbody>();
        setCountText();
        //CountText.text = "Count : " + count.ToString();
        Wintext.text = "";
    }
 void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
 Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);

       

       
      

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            setCountText();
        }
    }




  
void setCountText()
    {

 CountText.text = "Count: " + count.ToString();
        if (count >= 5)
        {
            Wintext.text = "Sup Nic, You won";
        }
    }

}
